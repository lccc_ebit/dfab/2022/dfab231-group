# Mechanical and Machine Design Group Project DFAB231
![](../images/dfab231.grouppictures/1.png)
![](../images/dfab231.grouppictures/2.png)
## Introduction

To receive credit for this lab, our instructors gave us the task of utilizing the design process to create “A Machine That Creates”.  We started by brainstorming ideas to see what direction Craig and I were thinking in.  While we were thinking creatively, the ideas that came up didn’t present any idea of a machine that created something, not just complete a task.  The idea of a 3D printer that printed with pancake batter came about.  While the idea met the criteria of a machine that created something, it was already a commercially redundant product.   What surprised both Craig and I was that the DFAB Lab has one of those stored in the back room.  For the sake of time, we came up with a simple idea for a robot that would meet the criteria, but in a creative way.  

Our robot will be equipped with a blue LED that’ll move in a desired toolpath, meanwhile one of us will be taking slow shutter speed images of the LED in motion.  Below are some images portraying what our machine is designed after, and what images slow shutter speeds capture.


## Programming & Electronics

### Heath Hasselbusch
I was tasked with the programming and electrical part of the project.  I played around with G-coding and the axles to experiment with what changes to the g-code effected how the machine ran.  I came up with a simple code that moved the axles back and forth at different speeds: 

<iframe width="560" height="315" src="https://www.youtube.com/embed/DYYoplo8vWs" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
 
My next step after this was to create a toolpath of the LCCC script using Aspire and CorralDraw.  For this step, I exported the image into CorralDraw to create the vectors that’ll come out to be our toolpath in Aspire. 

![](../images/dfab231.grouppictures/lccclogo.png)
![](../images/dfab231.grouppictures/4.png)
![](../images/dfab231.grouppictures/5.png)
	
  As you can see in the images above, I had to do a centerline trace of the script to get the path I wanted to trace, so what you see in the left image is the process of copying the original image, converting the copied image to a bitmap, and the image on the right shows how that line is converted into a toolpath. 
  
   The code is too big for me to put a picture of it in here, but I also had to insert M3 and M5 codes to turn the LED on/off. I used a blue LED, which can handle between 3.0V-3.2V.  With this in mind, I had to adjust the voltage so the LED didn’t burn out, which was just a matter of changing the spindle (S) value after the M3 code.   I set my value to 600, equivalent to 3.0V when the LED is ON.
 
 ![](../images/dfab231.grouppictures/arduinohookup.png)

Shown above is the Arduino V3.00 programmer.  Those 2 big motors control our X and Y axles.  Those 4-pins connect to the axles, and on the right, you can see I have a connection made to the Z+ pin for our LED.  I was initially experimenting with the spindle enable (SpnEn) spindle direction (SpnDir) and coolant enable (CoolEn) pins to control the LED, but none of those inputs worked.  I received a tip from an instructor who said to try the Z+ pin, and that seemed to do the trick.  

Here's a video of the program running. Notice the LED turns on, indicating when the machine starts running, and turns of when it indicates the program is complete, the LCCC scripture that is.

 <iframe width="560" height="315" src="https://www.youtube.com/embed/wHuppb07Bqc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

If you payed extra close attention, you'd see the spindle jumps from one auto-generated point, to the start of the next letter at another random location. This was a big problem, as this would cause the picture to process as just a giant blur with no evidence of text being made (or there would at least be scribbl-ish marks runinng across the text).  It also took longer to do this jump, rather than following the cursive nature of the text.  

## Lights…Camera…Action!

So now that we had our machine in working condition, it was time to put it to the real test.  Initially we took some pictures in the DFAB lab, however it was too bright to capture only the led’s path, and the machine was clearly visible in the pictures.  We then decided to go to a room where we could take pictures in complete darkness.  One thing we didn’t account for this time around was how bright the led was.  Because of the toolpaths size and perspective of where Craig’s phone was taking pictures from, the first bunch of photos were just emitting bright blue light.  

To capture the text correctly, we needed to lessen the voltage being transmitted to the led.  Initially the set value (s) was at 600 (units), equivalent to around 3.0V of electricity.  I experimented with lowering the voltage being emmited to the led, in theory that the dimmer the light is, the easier it’ll be for the camera to capture the path.  Sure enough, the value (s50) gave us this image:

![](../images/dfab231.grouppictures/blurrylccc.png)

I then put the value of (s) down to 1, which I can imagine is sending barely any voltage to the led, and here is how the image turned out:

![](../images/dfab231.grouppictures/clearlccc.jpg)

Clearly the biggest difference between these 2 images are the eligibility of the text, and the light surrounding the text.  The blurriness of the text and how bright the blue background light are directly proportional to the voltage being emitted to the led.  If we could’ve gone any dimmer we would’ve, but just because the led looks bright in the picture doesn’t mean it was bright in real time.

I definitely think using the blue led enhanced how this turned out.  LCCC’s school colors are blue and white, so the contrast of this really adds that special effect. 

# Mechanical Assembly by Craig Eckhart

My part was to build the machine using the files and patterns from the website [](http://mtm.cba.mit.edu/2014/2014_mmtm/). I downloaded the DXF cut files for use with a small format laser cutter (2' by 1') for use with the class cutter "Minnie". 
![](../images/project/1.jpg)
![](../images/project/2.jpg)

I sourced the required cardboard with the help of the lab assistant, and started working on the computer to get ready to cut.

![](../images/project/3.jpg)

I ended up using the newer fullsize laser cutter "Maxine". This is the first time using "Maxine" so I needed help with the settings for the cut and score lines. The lab assistant helped once again with that.

![](../images/project/4.jpg)

 The first "stage" I had used the small cardboard cut file and cut most of the pieces out.
 
![](../images/project/5.jpg)
 
 The main structure was seperated into unstable pieces when cut and assembled, so I used the large piece of cardboard file for the main structure pieces.

![](../images/project/6.jpg)
![](../images/project/7.jpg)

I started assembling the moveable inner structure.

![](../images/project/8.jpg)

I then moved on to the main structure, the way the it is designed to be cut seems to be incorrect as I needed to trim the long sides so that the inner moveable structure can move freely.

![](../images/project/9.jpg)

Once I test fit everything I started installing the motor that would be used and assembled the first "stage"

![](../images/project/10.jpg)
![](../images/project/11.jpg)

I cut the second "stage" and assembled it the same way as the first one. Once that was done I tested it out by hand before the motor was hooked up.

[](https://youtube.com/shorts/kiTrZVtZ2wM?feature=share)

I finished mounting the motor to the second stage, after mounting the LED into the moveable structure I bolted the main structure of the second "stage" to the moveable structure of the first "stage" forming a cross. I did allow for the extra weight of the motor, so it was not even, but offset a little.

![](../images/project/12.jpg)
![](../images/project/13.jpg)

The structure was complete at that point. Then it was time to connect it to the electronic part that Heath put together and programmed. First thing was to verify that the LED worked, Then it was on to test the complete set up.

![](../images/project/14.jpg)
![](https://youtube.com/shorts/yMLk35VZuUE)

The first attempt in the room to get a picture was too light. We needed to make sure that the room was darker than we had it.

![](../images/project/15.jpg)
![](../images/project/16.jpg)
 I did add the videos to the files section on the repository.
 
Heath summarized the rest of the project above.
